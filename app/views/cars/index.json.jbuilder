json.array! @cars do |car|
  json.partial! 'cars/car', car: car
end