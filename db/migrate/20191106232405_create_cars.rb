class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :title, required: true
      t.string :photo_url, required: true
      t.text :description, required: true
      t.decimal :price, precision: 2, required: true

      t.timestamps
    end
  end
end
