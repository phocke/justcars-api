require 'faker'

puts "Seeding database with cars"
50.times do
  Car.create(
    title: "#{Faker::Vehicle.make_and_model} #{Faker::Vehicle.color} #{Faker::Vehicle.car_options.join ' | '}",
    photo_url: Faker::LoremPixel.image(category: "transport", size: '640x480'),
    description: "#{Faker::Hipster.sentences.join ''} \n#{Faker::Address.full_address} \n#{Faker::Quote.famous_last_words}",
    price: Faker::Number.between(from: 20_000, to: 500_000))
end
puts "Cars seeded"

